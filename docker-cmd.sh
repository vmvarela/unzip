#!/bin/bash

EXCLUDEOPT=
[ x"${EXCLUDE}" != "x" ] && EXCLUDEOPT="--exclude=${EXCLUDE}"
cd ${UNZIPPED_PATH}
inotifywait -q -m -eclose_write -emoved_to ${EXCLUDEOPT} --format "%w%f" ${ZIPFILES_PATH} | while read FILENAME; 
do
         unzip -jo "${FILENAME}"
done

