FROM		debian:jessie
MAINTAINER	Victor M. Varela <v.varela@neartechnologies.com>

ENV		ZIPFILES_PATH	/zipfiles
ENV		UNZIPPED_PATH	/unzipped

RUN		apt-get update \
	&&	apt-get -y install --no-install-recommends inotify-tools unzip \
	&&	apt-get clean \
	&&	rm -rf /var/lib/{apt,dpkg,cache,log}

ADD		docker-cmd.sh	/docker-cmd.sh

CMD		["/docker-cmd.sh"]

